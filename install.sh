#!/bin/bash

cp vimrc ~/.vimrc
cp filetypes/* ~/.vim/ -R
cp autoload ~/.vim/ -R
cp bundle ~/.vim/ -R
