let $VIM = '~/.vim/'
let $VIMCPP = $VIM.'cpp.vim'
let $VIMPY = $VIM.'py.vim'

au BufRead,BufNewFile *.cpp,*.hpp       source $VIMCPP
au BufRead,BufNewFile *.cpp,*.hpp       source $VIMPY
" Pathogen
execute pathogen#infect()
	call pathogen#helptags() " generate helptags for everything in 'runtimepath'
	syntax on
"
"set smartindent
"set tabstop=4
"set shiftwidth=4
"set expandtab
"
"set clipboard=unnamedplus
"
"
"set ai
"set cin
"
"
"
"set showmatch
"set hlsearch
"set incsearch
"set ignorecase
"set ffs=unix,dos,mac
"set fencs=utf-8,cp1251,koi8-r,ucs-2,cp866
"set guioptions-=T
"set guioptions-=m 
"set t_Co=256
"
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'
"
""set cursorline
""set cursorcolumn
"highlight CursorColumn ctermbg=DarkGray
""highlight CursorLine ctermbg=DarkGray
"
"redraw
"
"
"
